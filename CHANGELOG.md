# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [8.34.0] - 2025-01-16

## [8.33.0] - 2024-12-18

## [8.32.0] - 2024-11-20

## [8.31.0] - 2024-10-23

## [8.30.0] - 2024-09-25

## [8.29.0] - 2024-08-29

## [8.28.0] - 2024-07-31

## [8.27.0] - 2024-07-05

## [8.26.0] - 2024-06-05

## [8.25.0] - 2024-05-08

## [8.24.0] - 2024-04-04

## [8.23.0] - 2024-03-08

## [8.22.0] - 2024-02-12

## [8.21.0] - 2024-01-12

## [8.20.0] - 2023-12-01

## [8.19.0] - 2023-09-27

## [8.18.0] - 2023-09-29

### Removed

- Remove eslint [`44cf6b6`](https://gitlab.open-xchange.com/documents/user-guide/commit/44cf6b6dcb6c16bcddb1a2270d30a31c0d16ca92)

## [8.17.0] - 2023-09-01

## [8.16.0] - 2023-08-03

## [8.15.0] - 2023-07-07

## [8.14.0] - 2023-06-07

## [8.13.0] - 2023-05-05

## [8.12.0] - 2023-04-05

## [8.11.0] - 2023-03-09

### Added

- en-US online help for 8.11 [`0641396`](https://gitlab.open-xchange.com/documents/user-guide/commit/0641396b1ea6ddb1593cfeba0dc191f4b0b5b48c)

## [8.10.0] - 2023-02-01

## [8.9.0] - 2023-01-12

### Added

- add to integration [`3e9c7d7`](https://gitlab.open-xchange.com/documents/user-guide/commit/3e9c7d7f5ce070413fb9dea55cbcbd172b2828f6)
- add dummy test for unit-test to package.json [`06dd3e5`](https://gitlab.open-xchange.com/documents/user-guide/commit/06dd3e594e4756636a3efb407c0551d8ab09b2b5)

### Removed

- removed obsolete file [`b7b5a18`](https://gitlab.open-xchange.com/documents/user-guide/commit/b7b5a18f7715eed0cca882fe8c3f96c0bd5ca361)

## [8.8.0]

[unreleased]: https://gitlab.open-xchange.com/documents/user-guide/compare/8.30.0...main
[8.9.0]: https://gitlab.open-xchange.com/documents/user-guide/compare/8.0.0...8.9.0

[8.11.0]: https://gitlab.open-xchange.com/documents/user-guide/compare/8.9.0...8.11.0

[8.18.0]: https://gitlab.open-xchange.com/documents/user-guide/compare/8.14.0...8.18.0

[8.30.0]: https://gitlab.open-xchange.com/documents/user-guide/compare/8.29.0...8.30.0
