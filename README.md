# user-guide

## Trigger publish Helm Chart

- Click "Run Pipeline" in the "Pipelines" view
- Change Branch if not ```main```
- Add the Variable ```PUBLISH_HELM_CHART``` with the value ```true```
- Click "Run Pipline"
