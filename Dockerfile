FROM registry.gitlab.open-xchange.com/appsuite/web-foundation/base-images/node_builder:latest AS builder

ARG APP_VERSION
ARG BUILD_TIMESTAMP
ARG CI_COMMIT_SHA

WORKDIR /app
COPY . /app

ENV PNPM_HOME=/root/.pnpm
ENV PATH=$PNPM_HOME:$PATH

RUN pnpm i -s -P && pnpm build && rm -rf /root/.local/share/pnpm

FROM registry.gitlab.open-xchange.com/appsuite/web-foundation/base-images/distroless/nginx:latest
COPY --from=builder /app/build /usr/share/nginx/html
